from django.shortcuts import render
from Aplicacion.models import Esclavos

"""Metodo para mandar a traer mediante 
objeto a la lista de los empleados"""

def list_empleados(request):
	empleados = Esclavos.objects.all()
	return render(request, template_name='Lista_empleados.html', context={'empleados': empleados})