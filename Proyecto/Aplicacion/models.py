from django.db import models


class Esclavos(models.Model):
	def _get_afp(self):
	  return round(self.sueldo*0.0725, 2)
	def _get_isss(self):
		if self.sueldo<1000:
			return round(self.sueldo*0.03, 2)
		else:
			return 30
	def _get_isr(self):
		aux = self.sueldo-(self.isss+self.afp)
		if aux<=472:
			return 0
		elif aux<=895.24:
			return round(((aux-472)*0.1)+17.67, 2)
		elif aux<=2038.10:
			return round(((aux-895.24)*0.2)+60, 2)
		else:
			return round(((aux-2038.10)*0.3)+288.57, 2)
	def _get_td(self):
		return round((self.afp+self.isss+self.isr), 2)
	def _get_sn(self):
		return round(self.sueldo-self.td, 2)
	#Atributos de la tabla Esclavos
	dui= models.CharField(max_length=10, default="123456789-1")
	nombres= models.CharField(max_length=30, default="Mario Alberto")
	apellidos= models.CharField(max_length=30, default="Benitez Cedillos")
	puesto= models.CharField(max_length=20, default="Ingeniero")
	sueldo= models.IntegerField(default="500")
	afp = property(_get_afp)
	isss = property(_get_isss)
	isr = property(_get_isr)
	td = property(_get_td)
	sn = property(_get_sn)
	def __str__(self):
		return self.nombres
	
	class Meta:
		ordering = ["apellidos"]