from django.contrib import admin
from Aplicacion.models import Esclavos

# Register your models here.

class EsclavosAmin(admin.ModelAdmin):
	"""docstring for  """
	list_display = ('nombres', 'apellidos', 'sueldo', 'puesto', 'dui')

"""Asociamos los modelos con las clases """
admin.site.register(Esclavos, EsclavosAmin)